﻿using System;

namespace Nullables
{
    class Nullables
    {
        static void Main(string[] args)
        {
            int? num1 = null;
            int? num2 = 45;

            double? num3 = new double?();
            double? num4 = 3.14157;
            double num5;
            double? num6 = null;
            double? num7 = 3.14157;

            bool? boolval = new bool?();

            Console.WriteLine("Nullsblesd at Show: {0}, {1}, {2}, {3}", num1, num2, num3, num4);
            Console.WriteLine("A Nullable boolean value: {0}", boolval);

            num5 = num6 ?? 5.34;
            Console.WriteLine("Value of num5: {0}", num5);
            num5 = num7 ?? 5.34;
            Console.WriteLine("Value of num5: {0}", num5);
            Console.ReadLine();
        }
    }
}
