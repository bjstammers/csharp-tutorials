﻿using System;

namespace TypeConversion
{
    class Conversion
    {
        static void Main(string[] args)
        {
            double d = 5673.74;
            int j = 75;
            float f = 53.005f;
            bool b = true;
            int i;

            // conversion examples - cast to int
            i = (int)d;
            Console.WriteLine(i);

            // conversion
            Console.WriteLine(j.ToString());
            Console.WriteLine(f.ToString());
            Console.WriteLine(d.ToString());
            Console.WriteLine(b.ToString());
            Console.ReadKey();
        }
    }
}
